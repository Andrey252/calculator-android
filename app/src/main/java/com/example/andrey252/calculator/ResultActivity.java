
package com.example.andrey252.calculator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity {

    private TextView firstOperandObj;
    private TextView secondOperandObj;
    private TextView resultAmountOperandsObj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firstOperandObj = (TextView) findViewById(R.id.first_operand_txt);
        secondOperandObj = (TextView) findViewById(R.id.second_operand_txt);
        resultAmountOperandsObj = (TextView) findViewById(R.id.result_amount_operands);

        String firstOperand = getIntent().getStringExtra("first_operand");
        String secondOperand = getIntent().getStringExtra("second_operand");
        String resultAmountOperands = checkValue(add(Double.parseDouble(firstOperand), Double.parseDouble(secondOperand)));

        outputAmountOperands(firstOperand, checkOperand(secondOperand), resultAmountOperands);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Метод выводит результат в активити, в виде суммы двух слагаемых и их результат.
     *
     * @param firstOperand         первый операнд.
     * @param secondOperand        второй операнд.
     * @param resultAmountOperands результат.
     */
    private void outputAmountOperands(String firstOperand, String secondOperand, String resultAmountOperands) {
        firstOperandObj.setText(firstOperand);
        secondOperandObj.setText(secondOperand);
        resultAmountOperandsObj.setText(resultAmountOperands);
    }

    /**
     * Метод проверяет корректность операнда, добавляя скобочки, если значение операнда отрицательное.
     *
     * @param operand исходный операнд.
     * @return преобразованный операнд.
     */
    private String checkOperand(String operand) {
        if (Double.parseDouble(operand) < 0) return "(" + operand + ")";
        return operand;
    }

    /**
     * Метод возвращает сумму своих аргументов
     *
     * @param firstOperand  первое слагаемое
     * @param secondOperand второе слагаемое
     * @return сумма {@code firstOperand} + {@code secondOperand}
     */
    private double add(double firstOperand, double secondOperand) {
        return firstOperand + secondOperand;
    }

    /**
     * Метод проверяет число, равна ли дробная часть числа нулю? Если истинно, то отбрасывается
     * дробная часть, иначе без изменений.
     *
     * @param result исходное число.
     * @return преобразованное число в виде строки.
     */
    private String checkValue(double result) {
        if (result % 1 == 0) return "" + (int) result;
        else return "" + result;
    }
}