/**
 * Приложение выполняет арифметическую операцию: сумму двух операндов.
 */

package com.example.andrey252.calculator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button resultButton;
    private EditText firstOperand;
    private EditText secondOperand;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultButton = (Button) findViewById(R.id.button_result);
        firstOperand = (EditText) findViewById(R.id.first_operand);
        secondOperand = (EditText) findViewById(R.id.second_operand);
        resultButton.setOnClickListener(this);
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_result:
                if (!buttonCheckClick(view, firstOperand)) break;
                if (!buttonCheckClick(view, secondOperand)) break;

                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                intent.putExtra("first_operand", firstOperand.getText().toString());
                intent.putExtra("second_operand", secondOperand.getText().toString());

                startActivity(intent);
                break;
        }
    }

    /**
     * Метод проверяет корректность введенного поля
     * @param view вьюшка
     * @param operandObj объект поля
     * @return возвращает {@code true} если данные в поле введены корректно, иначе {@code false}
     */
    public boolean buttonCheckClick(View view, EditText operandObj) {
        String operand = operandObj.getText().toString();
        if (operand.length() == 0) {
            operandObj.setError("Поле не может быть пустым!");
            return false;
        }
        return true;
    }
}